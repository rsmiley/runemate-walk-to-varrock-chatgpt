package com.mytroll.scripts.test;

import com.runemate.game.api.hybrid.GameEvents;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.location.navigation.basic.BresenhamPath;
import com.runemate.game.api.hybrid.location.navigation.basic.PredefinedPath;
import com.runemate.game.api.hybrid.location.navigation.web.Web;
import com.runemate.game.api.hybrid.location.navigation.web.WebPath;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.region.Region;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.LoopingBot;

public class test extends LoopingBot {

    private static final int VARROCK_TELEPORT_ID = 8009; // Varrock teleport tablet ID
    private static final int VARROCK_TELEPORT_SPELL_ID = 40; // Varrock teleport spell ID

    @Override
    public void onStart(String... args) {
        setLoopDelay(300, 600); // Set loop delay to a random value between 300ms and 600ms
    }

    private enum State {
        WALKING,
        TELEPORTING,
        WAITING,
        UNKNOWN
    }

    private State getStatus() {
        if (isInVarrock()) {
            return State.WAITING;
        } else if (Inventory.contains(VARROCK_TELEPORT_ID) || Skill.MAGIC.getBaseLevel() >= 25) {
            return State.TELEPORTING;
        } else if (Players.getLocal().getAnimationId() == -1 && !Players.getLocal().isMoving()) {
            return State.WALKING;
        } else {
            return State.UNKNOWN;
        }
    }

    private void login() {
       return;
    }

    private boolean isInVarrock() {
        Coordinate playerPosition = Players.getLocal().getPosition();
        Coordinate bottomLeft = new Coordinate(3160, 3385, 0); // Bottom-left corner of Varrock
        Coordinate topRight = new Coordinate(3265, 3487, 0); // Top-right corner of Varrock
        Area varrockArea = Area.rectangular(bottomLeft, topRight);
        return playerPosition != null && varrockArea.contains(playerPosition);
    }


    private void walkToVarrock() {
        System.out.println("Walking to Varrock...");
        Coordinate varrockCenter = new Coordinate(3213, 3424, 0); // Center of Varrock

        Web web = Traversal.getDefaultWeb();
        WebPath path = web.getPathBuilder().buildTo(varrockCenter);

        if (path != null) {
            path.step();
            Execution.delayUntil(() -> isInVarrock() || !Players.getLocal().isMoving(), 10000);
        } else {
            System.out.println("Unable to build a path to Varrock.");
        }
    }


    private void teleportToVarrock() {
        System.out.println("Teleporting to Varrock...");
        SpriteItem varrockTeleportTab = Inventory.newQuery().ids(VARROCK_TELEPORT_ID).results().first();

        if (varrockTeleportTab != null) {
            if (varrockTeleportTab.interact("Break")) {
                Execution.delayUntil(() -> isInVarrock(), 10000);
            } else {
                System.out.println("Failed to use the Varrock teleport tablet.");
            }
        } else if (Skill.MAGIC.getBaseLevel() >= 25 && Skill.MAGIC.getCurrentLevel() >= 3 && hasRequiredRunes()) {
            castVarrockTeleport();
        } else {
            System.out.println("Cannot teleport to Varrock. Insufficient level, runes, or teleport tablets.");
        }
    }

    private boolean hasRequiredRunes() {
        return Inventory.containsAllOf("Air rune", "Fire rune", "Law rune");
    }

    private void castVarrockTeleport() {
        InterfaceComponent spellbookIcon = Interfaces.newQuery().containers(593).actions("Cast").results().first();
        InterfaceComponent varrockTeleportSpell = Interfaces.newQuery().containers(218).actions("Cast").texts("Varrock Teleport").results().first();

        if (spellbookIcon != null && spellbookIcon.isVisible() && spellbookIcon.interact("Cast")) {
            Execution.delayUntil(() -> varrockTeleportSpell != null && varrockTeleportSpell.isVisible(), 2000);

            if (varrockTeleportSpell != null && varrockTeleportSpell.isVisible() && varrockTeleportSpell.interact("Cast")) {
                Execution.delayUntil(() -> Players.getLocal().getAnimationId() == -1, 10000);
            } else {
                System.out.println("Failed to cast Varrock Teleport.");
            }
        } else {
            System.out.println("Failed to open the Magic interface.");
        }
    }


    @Override
    public void onLoop() {
        State state = getStatus();
        System.out.println("Current state: " + state);

        switch (state) {
            case WALKING:
                walkToVarrock();
                break;
            case TELEPORTING:
                teleportToVarrock();
                break;
            case WAITING:
                Execution.delay(1000); // Wait for 1 second before checking the state again
                break;
            case UNKNOWN:
                System.out.println("Unknown State");
                break;
        }
    }
}
